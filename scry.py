#! /usr/bin/python3

import json
import requests
import time
import sys
import textwrap
import argparse

WIDTH = 80


def wrap_text(text):
    return '\n'.join(textwrap.wrap(text, width=WIDTH))


def web_api_call(url):
    time.sleep(0.1)
    return requests.get(url).json()


def is_type(card, type):
    return type in card['type_line'].lower()


def build_card_dict(card):
    dic = {
        'is_creature': False,
        'is_planeswalker': False,
        'name': card['name'],
        'mana_cost': "" if "mana_cost" not in card else card['mana_cost'],
        'typeline': card['type_line'],
        'oracle_text': "" if "oracle_text" not in card else card['oracle_text'],
        'flavor_text': "" if "flavor_text" not in card else card['flavor_text']
    }
    if is_type(card, 'creature'):
        dic['is_creature'] = True
        dic['power'] = card['power']
        dic['toughness'] = card['toughness']
    elif is_type(card, 'planeswalker'):
        dic['is_planeswalker'] = True
        dic['loyalty'] = card['loyalty']

    return dic


def scryfall_data(name):
    url_base = "https://api.scryfall.com/cards/named?fuzzy="
    card = web_api_call(url_base + name)
    if card['object'] == "error":
        return []

    if card['layout'] in ['normal', 'saga', 'leveler']:
        return [build_card_dict(card)]
    elif card['layout'] in ['adventure', 'split', 'transform', 'flip']:
        result = []
        for c in card['card_faces']:
            result.append(build_card_dict(c))
        return result


def scry_multi(lst, f):
    result = []
    for l in lst:
        r = f(l)
        for i in r:
            result.append(i)

    return result


def scryfall_price(name):
    url_base = "https://api.scryfall.com/cards/named?fuzzy="
    card = web_api_call(url_base + name)
    if card['object'] == "error":
        return []

    return [card['prices']]


def print_price(prices, in_json=False, show_flavor=True):
    if in_json:
        print(json.dumps(prices))
    else:
        for k, v in prices.items():
            if v is not None:
                padding = ''.join((9 - len(k)) * [" "]) + ''.join((6 - len(v)) * [" "])
                print("".join([k.upper(), ":", padding, v]))


def scryfall_rulings(name):
    url_base = "https://api.scryfall.com/cards/named?fuzzy="
    card = web_api_call(url_base + name)
    if card['object'] == "error":
        return []

    rulings = web_api_call(''.join(["https://api.scryfall.com/cards/", card["id"], "/rulings"]))

    return [rulings["data"]]


def print_rulings(rulings, in_json=False, show_flavor=True):
    if in_json:
        print(json.dumps(rulings))
    else:
        for r in rulings:
            print(''.join([wrap_text(''.join([r['published_at'], ': ', r['comment']])), "\n"]))


def cprint(lst, f, in_json=False, show_flavor=True):
    f(lst[0], in_json, show_flavor)
    for d in lst[1:]:
        print("\n" + ''.join(['/'] * WIDTH))
        f(d, in_json, show_flavor)


def read_card_file(filename):
    with open(filename) as fp:
        return [l.strip() for l in fp.readlines()]


def print_card(dic, in_json=False, show_flavor=True):
    if in_json:
        print(json.dumps(dic))
    else:
        spacing = "".join(max(WIDTH - (len(dic['name']) + len(dic['mana_cost'])), 0) * [' '])
        print("".join([dic['name'], spacing, dic['mana_cost']]))
        print(dic['typeline'] + '\n')
        print('\n'.join(textwrap.wrap((dic['oracle_text']), width=WIDTH, replace_whitespace=False)))
        if show_flavor:
            print('\n\n' + '\n'.join(textwrap.wrap((dic['flavor_text']), width=WIDTH)))
        if dic['is_creature']:
            print(''.join(
                ['\n', ''.join([' '] * (WIDTH - (len(dic['power']) + len(dic['toughness']) + 1))), dic['power'], "/",
                 dic['toughness']]))
        elif dic['is_planeswalker']:
            print(''.join(['\n', ''.join([' '] * (WIDTH - (len(' '.join(['Loyalty:', dic['loyalty']]))))),
                           ' '.join(['Loyalty:', dic['loyalty']])]))


if __name__ == '__main__':

    description = "A simple python script that pulls basic card MTG card \
    data from the scryfall api and displays it in the terminal."

    parser = argparse.ArgumentParser(description=description)

    # Input types
    parser.add_argument("card_name", metavar='C', action="store", nargs="*",
                        help="Name of the card to fetch of scryfall api. Uses fuzzy search.")
    parser.add_argument("-F", metavar="F", action="store", nargs=1, dest="file",
                        help="Read card names from a given file.")

    # Output modality
    parser.add_argument("-j", action="store_true", dest="ret_json", help="Return the data as a json object.")
    parser.add_argument("-f", action="store_false", dest="show_flavor", help="Supress flavor text in output.")

    # Output types
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-p", action="store_true", dest="price", help="Fetch the current prices of the given card.")
    group.add_argument("-r", action="store_true", dest="show_rulings", help="Fetch the rulings of the given card.")

    if len(sys.argv) < 2:
        parser.print_help()
        parser.exit(0)

    args = parser.parse_args()

    if args.price:
        f = scryfall_price
        p = print_price
    elif args.show_rulings:
        f = scryfall_rulings
        p = print_rulings
    else:
        f = scryfall_data
        p = print_card

    lst = f(" ".join(args.card_name)) if args.file is None else scry_multi(read_card_file(args.file[0]), f)

    if len(lst) == 0:
        print("Either the name is too generic or the card was not found. Please refine search.")
    else:
        cprint(lst, p, args.ret_json, args.show_flavor)

    exit(0)
